#!/bin/bash                                                                                                                                                             
. /etc/bashrc

# Change the CONFIG and OUT_FOLDER to the folder you want to the output root files to go to (you may need to create the folder first)
#You may also need to change the name of the rootfile within the code for example TestbeamAnalysis_ to the name set up in your code


# command line input is ./run_tang.sh runstart runend first_threshold 
# for example ./run_tang.sh 470 478 30 


CONFIG=LSStar.cfg 
OUT_FOLDER=rootFiles  

START=$1
END=$2
threshStart=$3

counter=$threshStart
s=$START

under="_"


for ((run_num = $START; run_num <=$END; run_num++)); do

    echo "Currently running over: " $run_num
    if [ $run_num -gt $s ]    
	then 
	counter=$[ $counter +10 ]
    fi
 
    #needed for the first compile 
#    ./make_testbeam_analysis.sh --config $CONFIG  --input Ntuple_LS_000$run_num$under$counter.root

    #can be used after the first compile and creating of TANG.exe 

    ./TANG.exe --config $CONFIG  --input Ntuple_LS_000$run_num$under$counter.root
    
    echo "The Threshold is now: " $counter 

    mv TestbeamAnalysis_LS_000$run_num$under$counter.root  $OUT_FOLDER/TestbeamAnalysis_LS_000$run_num$under$counter$under.root

    echo "New Output file name: " $OUT_FOLDER/TestbeamAnalysis_LS_000$run_num$under$counter$under.root

done

