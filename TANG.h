// ROOT includes
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TEfficiency.h"
#include "TGraphAsymmErrors.h"
#include "TObjArray.h"
#include "TEnv.h"
#include "TMath.h"

// C++ includes
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <math.h>
#include <algorithm>

using namespace std;

int total_events = -10;

vector<double>* x_hit = 0;
int event_nr_hit;

vector<double>* x_track = 0;
vector<double>* y_track = 0;
vector<double>* chi2_track = 0;
vector<double>* ndf_track = 0;
int event_nr_track;

vector<double>* nStrip;

int event_nr_cl;


vector<double>* x_zs = 0;
int event_nr_zs;

map<string,int>* headersInt = 0;
map<string,string>* headersString = 0;

int timing = -10;

void fatal(string msg){
  cout << "ERROR: " << msg << endl;
  abort();
}

double x_trackDUT = -10.;
double y_trackDUT = -10.;
double x_hitDUT = -10.;
double pix_hit_DUT = -10;
double pix_trk_DUT = -10.;
int    ASIC_trk_DUT = -10;
double x_residual = -10.;
double y_residual = -10.;
double distDUT = -10.;
double dist_trk_ctr = -10.;
double chi2 = -10.;
double ndof = -10.;
int    cl_size = -10;
int    delay = -10;
int    pixel_number = -100;
bool match_hit = false;
bool match_timing = false;

//desync stuff
int RAWBCID = -10;
int TTCBCID = -10;
bool desync = true;
bool goodevent = false;
int diff = -10;
int diff1 = -10;
int diff2 = -10;
int diff3 = -10;
int diff4 = -10;

std::vector<TString> vectorise(TString str, TString sep=" ") {
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}

std::vector<int> vectoriseI(TString str, TString sep=" ") {
  std::vector<int> result; std::vector<TString> vecS = vectorise(str);
  for (uint i=0;i<vecS.size();++i)
  result.push_back(atof(vecS[i]));
  return result;
}
