#include "TANG.h"

int main(int argc, char** argv) {
	
  if ( argc<2 ) {
    fatal("No arguments given! Can't continue.");
  }
  
  TH1::SetDefaultSumw2(kTRUE);

  // read input file
  TString input_file="";
  TString output_file="";
  TString settings_file;
  
  // loop over arguments
  // from teminal input ./make_testbeam_analysis.sh --config X.cfg  --input ../Ntuple_LS_run_threshold.root for example
  for(int i=1; i<argc; ++i){
    TString arg = TString(argv[i]);
    if(arg.BeginsWith("-")){
      arg.ReplaceAll("-","");
      if(arg=="config"){ settings_file = TString(argv[i+1]); ++i; continue; }
      else if(arg=="input"){ input_file = argv[i+1]; ++i; continue; }
      else {
        fatal(Form("Don't understand input argument %s",arg.Data()));
      }
    }
  }//there is a debug option that can be reintroduced here 

  // read run settings from config file
  TEnv* settings = new TEnv();
  settings->ReadFile(settings_file.Data(),EEnvLevel(0));


  int DUT_ID = settings->GetValue("DUT_ID",15);

  //  Config parsing
  //if there varibales are not set in the config the defualt value is assinged here 
  double max_chi2 = settings->GetValue("chi2cut",10.0); //currently it doesn't do anything. FIXME
  double strip_pitch = settings->GetValue("strip_pitch",0.0755);
  double max_dist = settings->GetValue("max_dist",0.2);
  
  // Timing selection (only for plots)
  int min_time = settings->GetValue("min_time",0);
  int max_time = settings->GetValue("max_time",50);

  // fiducial area cuts
  double pix_cut_min = settings->GetValue("pix_cut_min",-5.0);
  double pix_cut_max = settings->GetValue("pix_cut_max",2000.0);
  double y_cut_min = settings->GetValue("y_cut_min",-1000.0);
  double y_cut_max = settings->GetValue("y_cut_max",1000.0);
  
  // Noisy/inefficient pixels
  vector<int> bad_pixels = vectoriseI(settings->GetValue("bad_pixels",""));
  
  //opening the NTuple from the full reconstruction 
  TFile* f_in = TFile::Open(input_file,"read");
	
  // get file name for output
  TObjArray *strings = input_file.Tokenize("/");
  output_file = ((TObjString*)strings->Last())->GetString();
  output_file.ReplaceAll("Ntuple","TestbeamAnalysis");
  delete strings;
  cout << "Output file name: " << output_file << endl;
  
  // Retrieve TTrees
  TTree* t_tracks = (TTree*)f_in->Get("Tracks");
  TTree* t_hits = (TTree*)f_in->Get("Hits");
  TTree* t_zs = (TTree*)f_in->Get("ZeroSuppressed"); //not used for now
  TTree* t_head = (TTree*)f_in->Get("EventHeader");

  // Retrieve Branches  
  t_hits->SetBranchAddress("xPos",&x_hit);
  t_hits->SetBranchAddress("eventNumber",&event_nr_hit);
  t_tracks->SetBranchAddress("xPos",&x_track);
  t_tracks->SetBranchAddress("yPos",&y_track);
  t_tracks->SetBranchAddress("chi2",&chi2_track);
  t_tracks->SetBranchAddress("ndof",&ndf_track);
  t_head->SetBranchAddress("IntHeaders",&headersInt);
  t_head->SetBranchAddress("StringHeaders",&headersString);

  t_zs->SetBranchAddress("xPos",&nStrip);
  t_zs->SetBranchAddress("event_nr",&event_nr_cl);




  // create output root file and write trees/histograms/graphs
  TFile* f_out = TFile::Open(output_file,"recreate");

  // Output TTrees
  // create output ntuple (for final plots/histograms)
  TTree* t_out = new TTree("testbeam","testbeam");
  t_out->Branch("x_trk_DUT",&x_trackDUT);
  t_out->Branch("y_trk_DUT",&y_trackDUT);
  t_out->Branch("x_hit_DUT",&x_hitDUT);
  t_out->Branch("pix_hit_DUT",&pix_hit_DUT);
  t_out->Branch("pix_trk_DUT",&pix_trk_DUT);
  //t_out->Branch("pixel_centre",&pixel_centre); Commented lines are not working, but to be implemented
  t_out->Branch("dist_trk_ctr",&dist_trk_ctr);
  //t_out->Branch("ASIC_trk_DUT",&ASIC_trk_DUT);
  t_out->Branch("dist_DUT",&distDUT);
  t_out->Branch("cl_size",&cl_size);
  t_out->Branch("delay",&delay);

  // Output Histograms
  TH1D* ResidualHist = new TH1D("ResidualHist",";x_{hit}-x_{track} [mm];",200,-0.2,0.2);
  TH1D* ResidualHist_1 = new TH1D("ResidualHist_1",";x_{hit}-x_{track} [mm];",200,-0.2,0.2);
  TEfficiency* eff_x = new TEfficiency("eff_x","my efficiency;x;#epsilon",1280*10,0.5,1280.5);
  TEfficiency* eff_y = new TEfficiency("eff_y","my efficiency;y [mm];#epsilon",1000,-100,100);
  TEfficiency* eff_timing = new TEfficiency("eff_timing","my efficiency;delay [time bin];#epsilon",65,-15.5,49.5);
  TEfficiency* eff_event = new TEfficiency("eff_event","my efficiency;event number;#epsilon",120,0,240000);
  TEfficiency* eff_profile = new TEfficiency("eff_profile","my efficiency;Distance from strip center;#epsilon",20,-0.5,0.5);
  TEfficiency* cl_x = new TEfficiency("cl_x","cluster size;x;P(cluster>1)",1280*6,0.5,1280.5);
  TEfficiency* cl_y = new TEfficiency("cl_y","cluster size;y [mm];P(cluster>1)",1000,-100,100);
  TEfficiency* cs_timing = new TEfficiency("cs_timing","my efficiency;delay [time bin];P(cluster>1)",65,-15.5,49.5);



  total_events = t_hits->GetEntries();
  for (int eventn=0; eventn < total_events; eventn++){
	  
    if ( eventn % 5000 == 0 ) cout << Form("%5d / %6d events ( %7.1f%% )",int(eventn),int(total_events),100.0*eventn/total_events) << endl;
    
    t_tracks->GetEntry(eventn);
    t_hits->GetEntry(eventn);
    t_zs->GetEntry(eventn);
    t_head->GetEntry(eventn);

    if (x_track->size()!=1) continue; // Remove events with two tracks on reference plane or no tracks (to be fixed)
    
    distDUT = 1000.;
    for (int jj=0; jj<x_track->size();jj++){ //Loop is actually unnecessary
      x_trackDUT = x_track->at(jj);
      y_trackDUT = y_track->at(jj);
      chi2 = chi2_track->at(jj);
      ndof = ndf_track->at(jj);
      for (int ii=0;ii<x_hit->size();ii++){
        if(fabs(x_hit->at(ii)-x_trackDUT)<fabs(distDUT)) {
		  distDUT = x_hit->at(ii) - x_trackDUT;
		  x_hitDUT = x_hit->at(ii);
        }
      }
    }
    
	RAWBCID = -10;
	TTCBCID = -10;
	delay=-10;
	
	map<string,int>::iterator iter;
    for(iter = headersInt->begin(); iter != headersInt->end(); iter++) { //should check better if the the variables are non-empty
      if(iter->first == "RAWBCID") RAWBCID = iter->second;
	  if(iter->first == "TTC.BCID") TTCBCID = iter->second;	
    }
    
    diff = RAWBCID-TTCBCID;
    if (diff<0) diff = diff + 8; //should be done inline
	
    if(desync){
	  desync = false;
      continue;
	}

    bool goodevent = false;
    if (diff == diff1 && diff == diff2 && diff == diff3 && diff == diff4){
	  goodevent = true;
	} else { 
	  desync = true;
	}

    diff4 = diff3;
    diff3 = diff2;
    diff2 = diff1;
    diff1 = diff;
	
    if (!goodevent) continue;
    // position of hit and track in pixel coordinates (intra strip position)
    pix_hit_DUT = int(x_hitDUT/strip_pitch); //I don't think we need this
    pix_trk_DUT = x_trackDUT/strip_pitch;
    
    int trk_pixel = ceil(pix_trk_DUT);
    if (fabs(pix_trk_DUT-trk_pixel)>0.5) trk_pixel = trk_pixel-1;
    dist_trk_ctr = pix_trk_DUT - trk_pixel;

    map<string,string>::iterator iter2;
    for(iter2 = headersString->begin(); iter2 != headersString->end(); iter2++) {
      if(iter2->first == "PTDC.BIT") {
        TString n =iter2->second;
		timing = n.Atoi();
		n = n.Itoa(timing,2);
		delay = n.Sizeof();
		if (n.CountChar(49)==1) delay = delay-1; //this is due to some overflow that should be check for new data
		}
    }
    
    match_hit = false;
    if(fabs(distDUT) < max_dist) {
	  match_hit = true;		
	}
	
	// Track and hit position cut
    if ( pix_trk_DUT < pix_cut_min || pix_trk_DUT > pix_cut_max || y_trackDUT < y_cut_min || y_trackDUT > y_cut_max ) continue;
    
    // Chi2 cut
    if(chi2/ndof > max_chi2) continue;
    // Timing requirement for plots
    match_timing = false;
    if (delay >= min_time && delay <= max_time) match_timing = true;
    
    // Noisy/inefficient pixel remover
    bool bad_pixel=false;
    for ( const auto& pixel : bad_pixels ) {
      if ( fabs(pixel-pix_trk_DUT) <2 ) {
        bad_pixel=true;
        break;
      }
    }
    if (bad_pixel) continue;
    
    if (match_hit && match_timing) ResidualHist->Fill(distDUT);
    eff_timing->Fill(match_hit, delay);
    
    if(match_timing){
		eff_x->Fill(match_hit, pix_trk_DUT);
		eff_y->Fill(match_hit, y_trackDUT);
		eff_profile->Fill(match_hit, dist_trk_ctr);
		eff_event->Fill(match_hit, event_nr_hit);
	}
  



    // cluster size - clusters associated to a track 
    cl_size = 0;
	
    for ( int k=0; k<nStrip->size(); ++k){
      if(abs(nStrip->at(k) - (pix_hit_DUT-0.5)) < 3) cl_size++;
    }
    t_out->Branch("cl_size",&cl_size);


    //residual depending on cluster size, so far only for size 1 
    if (cl_size == 1 && match_hit && match_timing) ResidualHist_1->Fill(distDUT); // might not yet to be correct -- too many entries


    t_out->Fill();
  } //end of running over events 


  
  f_out->cd();
  t_out->Write();

  ResidualHist->Write();
  ResidualHist_1->Write();
  eff_x->Write();
  eff_y->Write();
  eff_timing->Write();
  eff_profile->Write();
  eff_event->Write();
  
  f_out->Close();
  delete f_out;

  return 1;
}
