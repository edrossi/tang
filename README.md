# TANG

Test beam Analysis for New GBL for ATLAS ITk Strip Test Beams

## Rough instruction on using TANG 
You need to be using root6 for this to work 

The code comes in two parts TANG.cxx and plot_efficiency.py 

- TANG.cxx reads in the NTuple created from the final step of EUTelescope. Hit and Track information etc are read in and some basic plots are made and ouputted to a new root file.
- plot_efficiency.py reads in the new root file and measures the efficiency and applys the fits


The inital command to run TANG.cxx is:

```
./make_testbeam_analysis.sh -config LSStar.cfg -input Path/NTuple.root 
```

- make_testbeam_analysis.sh complies TANG.cxx and only needs to run when there has been changes to TANG.cxx other wise ./TANG.exe to make the runtime shorter 
- LSStar.cfg has some tracking cuts that can be changed, but for a perpendicular scan the cuts here should be fine 
- Path/NTuple.root is the path to the root file outputted by the EUTelescope final step (full reconstruction or fitGBL)

There is a script called run_tang.sh that can be used to run TANG.cxx over a full threshold scan or automate running over x number of runs. For this you have the option of moving the output rootfiles to a folder. 

command line input is:
```
./run_tang.sh runstart runend first_threshold                                             
```
for example:
```
./run_tang.sh 470 478 30      
```
Not that this only works for thresholds that increment in steps of 10

Now that you have the output histograms from TANG.cxx you need to run plot_efficiency.py. 
- For this script to run, you also need in the same folder input_files.py and plot_tools.py 
- You need to create a folder called "plots" (see input_plots.py), this is where the output plots from plot_efficiency will go. 
    - When you rerun this script for a different scan you will need to rename the old directory called plots to avoid files being over written 
- In plot_tools.py, you need to input the correct calibration constants [pp]

Once you have everything above you should only need to run the plot_efficiency.py script 
```
python plot_efficiency.py
```
The output efficiency plots should be in the "/plots" folder


TANG.cxx is a basic analysis script that needs to be expanded for other analysis and can be improved. Please if you consider committing changes if you feel you additions will be useful for the wider strip testbeam community. 
The code is based off this version: https://gitlab.cern.ch/miqueits/TestbeamAnalysis .
Some components of the code are still to be copied over TANG.cxx and impelented for ABCStar testbeams